//funcao anonima imediatamente invocada
//IIFE
(function(a,b){
    console.log(a+b)
    console.log(`bla ${a +b}`)
})(1,4);

//arrow imediatamente invocada:

(()=>{
    console.log('arrow!')
})();