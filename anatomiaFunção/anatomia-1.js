
//function expression:
//modo "comum de criar funções"
function bla(){
    console.log("bla bla!")
    return () => {console.log("bla bla!")}
}

x = bla();
console.log('teste');

console.log(x);