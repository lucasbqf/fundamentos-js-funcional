// uma function expression
const inc = function(n){
    return n+1
}

//função arrow é sempre anonima
const inc2 = (n) => {
    return n+1
}

//função arrow apenas com um parametro
//nao precisa utilizar ()
const inc3 = n => {
    return n+1
}

//aso a função seja pequena/ é possivel 
//omitir o return, (pode ter mais parametros)
const inc4 = n => n+1

//pode colocar funcões sem problemas
const print = x => console.log(x)

console.log(inc(1))
console.log(inc2(5))
console.log(inc3(10))
console.log(inc4(15))

print(inc(1))