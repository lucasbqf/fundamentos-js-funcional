function logParams(a,b,c){
    console.log(a,b,c)
}
//normalmente
logParams(1,2,3)
//nao causa erro caso falte parametros
logParams(1,2)
//nao causa erro caso parametros nao declarados
logParams(1,2,3,4)

function asumb(a,b){
    console.log(a+b)
}
//nao gera erro se nao passor todos parametros
//só informa que nao é um numero o.o
asumb(1)

//porem voce pode de predefinir valores...
//dentro da declaração da função o.0

function bsuma(b = 1,a=4){
    console.log(a+b)
}

bsuma()
bsuma(2)


console.log('-------------------------')
//qualquer numero de parametros
//com um array (totalmente normal)
function logNums(nums){
    for (let n of nums){
        console.log(n)
    }
}

logNums([1,3,5])

//para n numeros sem que o parametro seja um array
//(ele ainda é, mas não é preciso mandar como array)
function newLogNums(...nums){
    for (let n of nums){
        console.log(n)
    }
}
console.log('----------nova função---------------')
newLogNums(1,2,3,4,5)


  