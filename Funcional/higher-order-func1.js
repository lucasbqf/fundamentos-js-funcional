/**
 * são funções que operao outras funcao
 * pegando estas como argumento ou mesmo
 * retornando uma função
 */

function run(...funcs){
    for(const fn of funcs)
        fn()
}

function ola(){
    console.log('ola')
}
function mundo(){
    console.log('mundo!')
}

run(ola,mundo)