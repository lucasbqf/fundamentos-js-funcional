//map.

const numeros = [1,2,3,4,5]


//função para duploca os numeros
const fnDobra = (el)=>el*2

//função para negativar os numeros
const fnNeg = (el) => -Math.abs(el)

//aplica as duas funcoes
resultado = numeros.map(fnDobra).map(fnNeg)


console.log(resultado)


//filter:
//basicamente realiza um if em um array, passando um metodo, se o elemento passa no teste, é retornado no novo array

// testa se o elemento é par
const fnPar = (el) => el % 2 == 0

result = numeros.filter(fnPar)

console.log(result)

//reduce, aplica uma função entre todos os elementros de um array realizando uma operação entre eles
//o primeiro parametro é o acumulador, e o segundo é o elemento.

const soma = (total,el) =>{
    return total += el 
}

result = numeros.reduce(soma)

console.log(result)